class App {
  constructor() {
    let usernames = localStorage.getItem(`bwusernames`) || ``
    usernames = usernames.split(`,`)

    let textarea = document.querySelector(`textarea`)
    textarea.value = ``
    for (let name of usernames) {
      textarea.value += textarea.value === `` ? name : `\n${name}`
    }

    this.teams = {
      red: {
        players: [],
        bedTimer: 0,
        hasBed: true,
        eliminated: false,
        name: `Red Team`,
        kills: 0
      },
      yellow: {
        players: [],
        bedTimer: 0,
        hasBed: true,
        eliminated: false,
        name: `Yellow Team`,
        kills: 0
      },
      green: {
        players: [],
        bedTimer: 0,
        hasBed: true,
        eliminated: false,
        name: `Green Team`,
        kills: 0
      },
      blue: {
        players: [],
        bedTimer: 0,
        hasBed: true,
        eliminated: false,
        name: `Blue Team`,
        kills: 0
      }
    }
    this.playerNames = []

    this._secondsElapsed = 0
    this.timeElement = document.querySelector(`.time`)

    this.playerListElement = document.querySelector(`.player_list`)
  }
  startTimer() {
    setInterval(() => {
      if (this.gameEnded) return
      this._secondsElapsed++

      this.timeElement.innerText = this.prettyTime

      // if (this._secondsElapsed >= 10) {
      if (this._secondsElapsed >= 20 * 60) {
        let endButton = document.querySelector(`.game_end`)
        endButton.classList.remove(`outline_button`)
      }
    }, 1000)
  }
  get prettyTime() {
    return this._prettyTime(this._secondsElapsed)
  }
  _prettyTime(time) {
    let minutes = Math.floor(time / 60)
    let seconds = String(time % 60)
    return `${minutes}:${seconds.padStart(2, `0`)}`
  }
  createGameStageList() {
    while (this.playerListElement.firstChild) {
      this.playerListElement.removeChild(this.playerListElement.firstChild);
    }

    for (let teamName of Object.keys(this.teams)) {
      let teamElement = document.createElement(`div`)
      let team = this.teams[teamName]
      teamElement.classList.add(`list_element`)
      teamElement.classList.add(`${teamName}_list_element`)
      teamElement.innerHTML = `<strong>${team.name}</strong>`
      if (team.hasBed) {
        teamElement.innerHTML += `<br>Bed Alive<br>`

        // bed destroyed button
        let bedDestroyedButton = document.createElement(`button`)
        bedDestroyedButton.classList.add(`bed_destroyed_button`)
        bedDestroyedButton.innerText = `Bed Destroyed`
        bedDestroyedButton.setAttribute(`data-team`, teamName)
        teamElement.appendChild(bedDestroyedButton)
      } else {
        teamElement.innerHTML += `<br>Bed Destroyed (${this._prettyTime(team.bedTimer)})`
      }
      teamElement.innerHTML += `<br>Kills: ${team.kills}<br><br>`
      for (let player of team.players) {
        let playerElement = document.createElement(`div`)
        let pointsWord = player.points == 1 ? `point` : `points`
        playerElement.innerText = `${player.name} (${player.points} ${pointsWord})`

        if (player.eliminated) {
          playerElement.classList.add(`player_eliminated`)
        } else {
          // kill button
          let inlineDefeatButton = document.createElement(`button`)
          inlineDefeatButton.classList.add(`defeat_button`)
          inlineDefeatButton.classList.add(`inline_button`)
          inlineDefeatButton.innerText = `K`
          inlineDefeatButton.setAttribute(`data-team`, teamName)
          inlineDefeatButton.setAttribute(`data-player`, player.name)
          playerElement.appendChild(inlineDefeatButton)
          // final button
          let inlineFinalButton = document.createElement(`button`)
          inlineFinalButton.classList.add(`final_button`)
          inlineFinalButton.classList.add(`inline_button`)
          inlineFinalButton.innerText = `F`
          inlineFinalButton.setAttribute(`data-team`, teamName)
          inlineFinalButton.setAttribute(`data-player`, player.name)
          playerElement.appendChild(inlineFinalButton)
          // bed destroy button
          let inlineBedButton = document.createElement(`button`)
          inlineBedButton.classList.add(`bed_button`)
          inlineBedButton.classList.add(`inline_button`)
          inlineBedButton.innerText = `B`
          inlineBedButton.setAttribute(`data-team`, teamName)
          inlineBedButton.setAttribute(`data-player`, player.name)
          playerElement.appendChild(inlineBedButton)
          // eliminated button if bed broken
          if (!team.hasBed) {
            let inlineElimButton = document.createElement(`button`)
            inlineElimButton.classList.add(`eliminated_button`)
            inlineElimButton.classList.add(`inline_button`)
            inlineElimButton.innerText = `E`
            inlineElimButton.setAttribute(`data-team`, teamName)
            inlineElimButton.setAttribute(`data-player`, player.name)
            playerElement.appendChild(inlineElimButton)
          }
        }

        teamElement.appendChild(playerElement)
      }
      this.playerListElement.appendChild(teamElement)
    }
    for (let button of document.querySelectorAll(`.bed_destroyed_button`)) {
      button.onclick = ev => {
        let buttonTeam = this.teams[ev.target.getAttribute(`data-team`)]
        buttonTeam.hasBed = false
        this.createGameStageList()
        setInterval(() => {
          if (buttonTeam.eliminated || this.gameEnded) return
          if (buttonTeam.bedTimer > 0 && buttonTeam.bedTimer % 60 === 59) {
            for (let player of buttonTeam.players) {
              if (!player.eliminated) {
                player.points++
              }
            }
          }
          buttonTeam.bedTimer++
          this.createGameStageList()
        }, 1000)
      }
    }
    for (let button of document.querySelectorAll(`.defeat_button`)) {
      button.onclick = ev => {
        let team = this.teams[ev.target.getAttribute(`data-team`)]
        team.kills++

        for (let player of team.players) {
          if (player.name === ev.target.getAttribute(`data-player`)) {
            player.points++
          }
        }
        this.createGameStageList()
      }
    }
    for (let button of document.querySelectorAll(`.final_button`)) {
      button.onclick = ev => {
        let team = this.teams[ev.target.getAttribute(`data-team`)]
        team.kills++

        for (let player of team.players) {
          if (player.name === ev.target.getAttribute(`data-player`)) {
            player.points += 3
          }
        }
        this.createGameStageList()
      }
    }
    for (let button of document.querySelectorAll(`.bed_button`)) {
      button.onclick = ev => {
        let team = this.teams[ev.target.getAttribute(`data-team`)]
        for (let player of team.players) {
          if (player.name === ev.target.getAttribute(`data-player`)) {
            player.points += 5
          }
        }
        this.createGameStageList()
      }
    }
    for (let button of document.querySelectorAll(`.eliminated_button`)) {
      button.onclick = ev => {
        let team = this.teams[ev.target.getAttribute(`data-team`)]
        for (let player of team.players) {
          if (player.name === ev.target.getAttribute(`data-player`)) {
            player.eliminated = true
          }
        }
        if (team.players.every(player => player.eliminated)) {
          team.eliminated = true
          let aliveTeams = []
          for (let [teamName, teamData] of Object.entries(this.teams)) {
            if (!teamData.eliminated) {
              aliveTeams.push(teamName)
            }
          }
          if (aliveTeams.length === 1) {
            this.gameEnded = true
            for (let player of this.teams[aliveTeams[0]].players) {
              if (!player.eliminated) {
                player.points += 10
              }
            }
          }
        }
        this.createGameStageList()
      }
    }
  }
}

document.addEventListener(`DOMContentLoaded`, () => {
  self.app = new App

  document.querySelector(`.game_start`).addEventListener(`click`, () => {
    app.playerNames = document.querySelector(`textarea`).value.split(`\n`)
    localStorage.setItem(`bwusernames`, app.playerNames.join(`,`))
    app.startTimer()
    document.querySelector(`.entry_stage`).style.display = `none`
    document.querySelector(`.names_stage`).style.display = `block`

    document.querySelector(`.username_team_select`).innerText = app.playerNames.shift()
  })
  document.querySelector(`.game_end`).addEventListener(`click`, () => {
    if (app.gameEnded) return
    app.gameEnded = true
    let teamDefeats = []
    for (let [teamName, teamData] of Object.entries(app.teams)) {
      teamDefeats.push(teamData.kills)
    }
    let highestDefeats = Math.max(...teamDefeats)
    for (let [teamName, teamData] of Object.entries(app.teams)) {
      if (teamData.kills >= highestDefeats) {
        for (let player of teamData.players) {
          if (!player.eliminated) {
            player.points += 10
          }
        }
      } else {
        for (let player of teamData.players) {
          player.eliminated = true
        }
      }
    }
    app.createGameStageList()
  })

  for (let button of document.querySelectorAll(`.team_button`)) {
    button.addEventListener(`click`, ev => {
      let team = ev.target.getAttribute(`data-team`)
      let player = {
        name: document.querySelector(`.username_team_select`).innerText,
        points: 0,
        eliminated: false
      }
      app.teams[team].players.push(player)

      if (app.playerNames.length > 0) {
        document.querySelector(`.username_team_select`).innerText = app.playerNames.shift()
      } else {
        document.querySelector(`.names_stage`).style.display = `none`
        document.querySelector(`.game_stage`).style.display = `block`
        app.createGameStageList()
      }
    })
  }
})
