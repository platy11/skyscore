class App {
  constructor() {
    let usernames = localStorage.getItem(`usernames`) || ``
    usernames = usernames.split(`,`)

    let playerInputElement = document.querySelector(`.team`)
    this.playerInputArea = document.querySelector(`.username_entry`)
    playerInputElement.children[0].value = usernames[0]
    for (let i = 0; i < 23; i++) {
      let element = playerInputElement.cloneNode(true)
      element.children[0].value = usernames[i + 1] || ``
      this.playerInputArea.appendChild(element)
    }

    this.playersAlive = new Map()
    this.playersEliminated = new Map()

    this._secondsElapsed = 0
    this.timeElement = document.querySelector(`.time`)

    this.playerListElement = document.querySelector(`.player_list`)
  }
  addPlayer(username) {
    this.playersAlive.set(username.trim(), 0)
  }
  startTimer() {
    setInterval(() => {
      if (this.gameEnded) return
      this._secondsElapsed++

      this.timeElement.innerText = this.prettyTime

      if (this._secondsElapsed >= 360) {
        let endButton = document.querySelector(`.game_end`)
        endButton.style.display = `inline-block`
      }
      if (this._secondsElapsed % 60 === 0 && this._secondsElapsed > 0) {
        for (let [player, score] of app.playersAlive) {
          this.addToScore(player, 1)
        }
        this.createGameStageList()
      }
    }, 1000)
  }
  get prettyTime() {
    let minutes = Math.floor(this._secondsElapsed / 60)
    let seconds = String(this._secondsElapsed % 60)
    return `${minutes}:${seconds.padStart(2, `0`)}`
  }
  createGameStageList() {
    while (this.playerListElement.firstChild) {
      this.playerListElement.removeChild(this.playerListElement.firstChild);
    }
    function createPlayerElement (player, score, eliminated, that) {
      let element = document.createElement('div')
      element.classList.add('player_item')
      let pointsWord = score === 1 ? `point` : `points`
      element.innerText = `${player} (${score} ${pointsWord})`
      element.setAttribute(`data-player`, player)
      that.playerListElement.appendChild(element)

      if (eliminated) {
        element.classList.add('player_eliminated')
        return
      }

      let registerDefeat = document.createElement('button')
      registerDefeat.classList.add('inline_button')
      registerDefeat.classList.add('defeat_button')
      registerDefeat.addEventListener(`click`, ev => {
        if (that.gameEnded) return
        let playerTarget = ev.target.parentNode.getAttribute(`data-player`)
        that.registerDefeat(playerTarget)
      })
      element.appendChild(registerDefeat)

      let registerEliminated = document.createElement('button')
      registerEliminated.classList.add('inline_button')
      registerEliminated.classList.add('eliminated_button')
      registerEliminated.addEventListener(`click`, ev => {
        if (that.gameEnded) return
        let playerTarget = ev.target.parentNode.getAttribute(`data-player`)
        that.registerEliminated(playerTarget)
      })
      element.appendChild(registerEliminated)
    }
    for (let [player, score] of this.playersAlive) {
      createPlayerElement(player, score, false, this)
    }
    for (let [player, score] of this.playersEliminated) {
      createPlayerElement(player, score, true, this)
    }
  }
  addToScore(player, amount) {
    this.playersAlive.set(player, this.playersAlive.get(player) + amount)
  }
  get defeatPointsBonus() {
    let playerCount = this.playersAlive.size
    if (playerCount > 8) {
      return 2
    } else if (playerCount > 5) {
      return 3
    } else {
      return 4
    }
  }
  get placingBonus() {
    let playerCount = this.playersAlive.size
    if (playerCount === 1) {
      return 8
    } else if (playerCount === 2) { // 3rd
      return 7
    } else if (playerCount === 3) { // 4th
      return 6
    } else if (playerCount === 4) { // 5th
      return 5
    } else { // 6th or lower
      return 2
    }
  }
  registerEliminated(player) {
    let playerScore = this.playersAlive.get(player)
    this.playersAlive.set(player, undefined)
    this.playersAlive.delete(player)
    this.playersEliminated.set(player, playerScore + this.placingBonus)
    if (this.playersAlive.size <= 1) {
      this.gameEnded = true
      for (let [winner, score] of this.playersAlive) {
        this.addToScore(winner, 10)
      }
    }
    this.createGameStageList()
  }
  registerDefeat(player) {
    // player defeats another, gains points
    this.addToScore(player, this.defeatPointsBonus)
    this.createGameStageList()
  }
}

document.addEventListener(`DOMContentLoaded`, () => {
  self.app = new App

  document.querySelector(`.game_start`).addEventListener(`click`, () => {
    for (let playerInputContainer of app.playerInputArea.children) {
      let username = playerInputContainer.children[0].value
      if (username) {
        app.addPlayer(username)
      }
    }
    app.startTimer()
    document.querySelector(`.entry_stage`).style.display = `none`
    document.querySelector(`.game_stage`).style.display = `block`
    app.createGameStageList()
  })
  document.querySelector(`.game_end`).addEventListener(`click`, () => {
    if (app.gameEnded) return
    app.gameEnded = true
    for (let [player] of app.playersAlive) {
      app.addToScore(player, 2)
    }
    app.playersEliminated = app.playersAlive
    app.playersAlive = new Map()
    app.createGameStageList()
  })
})
